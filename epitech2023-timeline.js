google.charts.load("current", { packages: ["timeline"] });
google.charts.setOnLoadCallback(drawChart);
var today = new Date();

function date(day, month, year) {
  return new Date(year, month - 1, day);
}

function start(day, month, year) {
  return date(day, month, year);
}

function end(day, month, year) {
  var d = date(day, month, year);
  d.setDate(d.getDate() + 1);
  return d;
}

function drawChart() {
  var container = document.getElementById('timeline-container');
  var chart = new google.visualization.Timeline(container);
  var dataTable = new google.visualization.DataTable();
  dataTable.addColumn({ type: 'string', id: 'Module' })
  dataTable.addColumn({ type: 'string', id: 'Project' });
  dataTable.addColumn({ type: 'date', id: 'Start' });
  dataTable.addColumn({ type: 'date', id: 'End' });
  var now = new Date(today.getFullYear(), today.getMonth(), today.getDate());
  dataTable.addRows([
    ['\0', 'Now', now, now],

    ['C++ Seminar', 'C++ Seminar', start(15, 01, 2020), end(02, 02, 2020)],
    ['Web Security', 'Call For Papers', start(27, 01, 2020), end(16, 02, 2020)],
    ['Web Security', 'SHODAN', start(17, 02, 2020), end(01, 03, 2020)],
    ['Concurrent Programming', 'The Plazza', start(06, 04, 2020), end(10, 05, 2020)],
    ['Computer Numerical Analysis', 'Groundhog', start(16, 03, 2020), end(12, 04, 2020)],
    ['Computer Numerical Analysis', 'Trade', start(13, 04, 2020), end(07, 06, 2020)],
    ['Français', 'Mission délicate: recadrer un collègue', start(27, 01, 2020), end(09, 02, 2020)],
    ['Français', 'Diaporama pour décrocher 1 million de dollars', start(17, 02, 2020), end(01, 03, 2020)],
    ['Français', 'Rédiger un bilan d\'expérience', start(02, 03, 2020), end(15, 03, 2020)],
    ['Functional Programming', 'Wolfram', start(24, 02, 2020), end(08, 03, 2020)],
    ['Functional Programming', 'Extension Lyon - Wolfram', start(09, 03, 2020), end(22, 03, 2020)],
    ['Functional Programming', 'Image Compressor', start(30, 03, 2020), end(03, 05, 2020)],
    ['Mathematics', '201yams', start(10, 02, 2020), end(23, 02, 2020)],
    ['Mathematics', '202unsold', start(24, 02, 2020), end(08, 03, 2020)],
    ['Mathematics', '203hotline', start(09, 03, 2020), end(22, 03, 2020)],
    ['Mathematics', '204ducks', start(23, 03, 2020), end(05, 04, 2020)],
    ['Mathematics', '205IQ', start(06, 04, 2020), end(19, 04, 2020)],
    ['Mathematics', '206neutrinos', start(20, 04, 2020), end(03, 05, 2020)],
    ['Mathematics', '207demography', start(27, 04, 2020), end(10, 05, 2020)],
    ['Mathematics', '208dowels', start(04, 05, 2020), end(17, 05, 2020)],
    ['Mathematics', '209poll', start(11, 05, 2020), end(24, 05, 2020)],
    ['Network And System Administration', 'S.N.A', start(03, 02, 2020), end(16, 02, 2020)],
    ['Network Programming', 'my_ftp', start(30, 03, 2020), end(19, 04, 2020)],
    ['Network Programming', 'my_teams', start(27, 04, 2020), end(31, 05, 2020)],
    ['Object-Oriented Programming', 'NanoTekSpice', start(03, 02, 2020), end(01, 03, 2020)],
    ['Object-Oriented Programming', 'Arcade', start(02, 03, 2020), end(05, 04, 2020)],
    ['Unix Programming - Instrumentation', 'strace', start(09, 03, 2020), end(22, 03, 2020)],
    ['Unix Programming - Instrumentation', 'ftrace', start(30, 03, 2020), end(26, 04, 2020)],
    ['Unix Programming - Memory', 'malloc', start(03, 02, 2020), end(16, 02, 2020)],
    ['Unix Programming - Memory', 'nm/objdump', start(17, 02, 2020), end(08, 03, 2020)],
    ['x86-64 Assembly', 'MiniLibC', start(10, 02, 2020), end(01, 03, 2020)],
    ['Year-End-Project - Indie Studio', 'Indie Studio', start(04, 05, 2020), end(14, 06, 2020)],
    ['Year-End-Project - Zappy', 'Zappy', start(11, 05, 2020), end(21, 06, 2020)]
  ]);


  chart.draw(dataTable, {
    timeline: {
      colorByRowLabel: true
    }
  });

  nowLine('timeline-container');

  google.visualization.events.addListener(chart, 'onmouseover', function (obj) {
    if (obj.row == 0) {
      $('.google-visualization-tooltip').css('display', 'none');
    }
    nowLine('timeline-container');
  })

  google.visualization.events.addListener(chart, 'onmouseout', function (obj) {
    nowLine('timeline-container');
  })
}

function nowLine(div) {

  //get the height of the timeline div
  var height;
  $('#' + div + ' rect').each(function (index) {
    var x = parseFloat($(this).attr('x'));
    var y = parseFloat($(this).attr('y'));

    if (x == 0 && y == 0) {
      height = parseFloat($(this).attr('height'))
    }
  })

  var nowWord = $('#' + div + ' text:contains("Now")');

  nowWord.prev().first().attr('height', height + 'px').attr('width', '1px').attr('y', '0');
  // add this line to remove the display:none style on the vertical line
  $('#' + div + '  text:contains("Now")').each(function (idx, value) {
    if (idx == 0) {
      $(value).parent().find("rect").first().removeAttr("style");
    } else if (idx == 1) {
      $(value).parent().find("rect").first().attr("style", "display:none;");
    }

  });
}

var repourl = "https://gitlab.com/epi-codes/Epitech-2023-Timeline";
$(document).ready(function () {
  $.getJSON(
    "https://gitlab.com/api/v4/projects/epi-codes%2fEpitech-2023-Timeline/repository/commits",
    function (json) {
      var msg, el, date;

      $("#changelog-container").empty();

      console.log(json);

      for (var i = 0; i < json.length; i++) {
        msg = json[i].message.split("\n");
        date = moment(json[i].created_at);
        el = $(`<p class="commit">
<a href="${repourl}/commit/${json[i].id}" target="_blank" class="commit-msg">${
          msg[0]
          }</a>
<span title="${date.format(
            "dddd, MMMM Do YYYY, h:mm:ss a"
          )}" class="commit-date">${date.fromNow()}</span>
</p>`);
        if (msg.length > 1) {
          for (var j = msg.length - 1; j >= 1; j--) {
            if (msg[j].length > 0) {
              el.addClass("expanded");
              el.find("a").after(`<span class="commit-desc">${msg[j]}</span>`);
            }
          }
        }
        el.appendTo($("#changelog-container"));
      }

      if (json.length <= 0) {
        $("#changelog-container").text("No commits !? xO");
      }
    })
    .fail(function () {
      $("#changelog-container").text("Error while loading changelog :'(");
    });

  function set_theme(dark) {
    var dark = dark || false;

    window.localStorage.setItem("dark", dark);

    if (dark) {
      $("body").addClass("dark");
      $("#switch").text("Switch to light");
    }
    else {
      $("body").removeClass("dark");
      $("#switch").text("Switch to dark");
    }
  }

  $("#switch").on("click", function () {
    set_theme(!$("body").hasClass("dark"));
    return false;
  })

  set_theme(window.localStorage.getItem("dark") == "true" ? true : false);
  setTimeout(function () {
    $('body').addClass('ready');
  }, 500);
});
